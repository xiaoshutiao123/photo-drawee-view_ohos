# PhotoDraweeView

## 项目介绍
+ 项目名称：PhotoDraweeView
+ 所属系列：openHarmony的第三方组件适配移植
+ 功能：多场景图片缩放移动处理，PhotoView自定义的Image，通过监听onTouch事件来处理滑动、双击、双指等事件，从而实现image的缩放移动。
+ 项目移植状态：主功能完成
+ 调用差异：系统api未提供自定义转场动画，现在的效果为系统默认的转场动画效果。
+ 开发版本：sdk6，DevEco Studio 2.2 Beta1
+ 基线版本：Releases v2.0.0

## 项目演示
|单张图片缩放|ViewPager图片缩放|
|:---:|:---:|
|<img src="https://gitee.com/chinasoft_ohos/PhotoDraweeView/raw/master/img/1.gif" width="75%"/>|<img src="https://gitee.com/chinasoft_ohos/PhotoDraweeView/raw/master/img/11.gif" width="75%"/>|

|列表图片缩放|
|:---:|
|<img src="https://gitee.com/chinasoft_ohos/PhotoDraweeView/raw/master/img/111.gif" width="75%"/>|

## 安装教程

1.在项目根目录下的build.gradle文件中
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```

2.在entry模块的build.gradle文件中
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:PhotoDraweeView:1.0.0')
   ......  
}
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

1. 引用har包中自定义PhotoView加载图片

```
<com.me.relex.photodraweeview.PhotoView
        ohos:id="$+id:img"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:layout_alignment="horizontal_center"/>
```

2. 如果PhotoView的父布局也有滚动操作，需要对滑动冲突做设置

(a) 如果父布局左右滑动

```
// 设置是否需要处理父布局左右滑动
PhotoView.isPageSlider(boolean);
// 设置是否图片临界监听回调
public void setOnEnableListener(OnEnableListener enableListener) {
        mEnableListener = enableListener;
}
```

(b) 如果父布局上下滑动

```
// 设置是否需要处理父布局左右滑动
PhotoView.isRecyclerView(boolean);
// 设置是否图片临界监听回调
public void setOnEnableListener(OnEnableListener enableListener) {
        mEnableListener = enableListener;
}
```

3. 设置缩放比例

```
void setScaleStep(float scaleStep)
```

4. 重置图片效果

```
void initPhotoView(Component component)
```

5. 更多关于不同需求的图片缩放要求可以参考本工程的entry模块

6. 未实现的功能点差异：

- Share Element 模块的转场动画未能同原项目一样(OpenHarmony不支持类似原项目的转场动画)

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

+ 1.0.0

## 版权和许可信息
+ Apache 2.0