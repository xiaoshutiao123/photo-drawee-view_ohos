/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.slice;

import com.me.relex.photodraweeview.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * SharedElement 展开后的 slice
 *
 * @since 2021-03-27
 */
public class SharedElementLaunchedAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "0000252584");

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_shared_element_launched);
        Image shareLaunchedImg = (Image) findComponentById(ResourceTable.Id_share_launch_img);
        shareLaunchedImg.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        HiLog.info(LABEL, "SharedElementLaunchedAbilitySlice.onClick()");
        terminate();
    }
}
