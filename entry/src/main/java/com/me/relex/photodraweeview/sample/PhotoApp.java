package com.me.relex.photodraweeview.sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * AbilityPackage
 *
 * @since 2021-03-27
 */
public class PhotoApp extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
