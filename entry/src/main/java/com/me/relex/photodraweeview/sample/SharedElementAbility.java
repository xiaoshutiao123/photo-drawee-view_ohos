package com.me.relex.photodraweeview.sample;

import com.me.relex.photodraweeview.sample.slice.SharedElementAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * SharedElementAbility
 *
 * @since 2021-03-27
 */
public class SharedElementAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SharedElementAbilitySlice.class.getName());
    }
}
