/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample;

import com.me.relex.photodraweeview.sample.provider.TitleMenuProvider;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * viewpager slice
 *
 * @since 2021-03-27
 */
public class CustomTitle extends DirectionalLayout implements Component.ClickedListener {
    private Image mBackImg;
    private Text mTitleTex;

    private OnBackClickListener onBackClickListener;
    private Image mMenuImg;
    private PopupDialog mPopupWindow;
    private MenuItemClickedListener mMenuItemClickedListener;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public CustomTitle(Context context) {
        super(context);
        initView(context);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet 自定义属性
     */
    public CustomTitle(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    /**
     * 构造函数
     *
     * @param context   上下文
     * @param attrSet   自定义属性
     * @param styleName 自定义样式
     */
    public CustomTitle(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    /**
     * 初始化布局
     *
     * @param context 上下文
     */
    private void initView(Context context) {
        Component layout = LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_custom_title, this, true);
        DependentLayout mLayout = (DependentLayout) layout.findComponentById(ResourceTable.Id_depenent_lauout);
        mBackImg = (Image) layout.findComponentById(ResourceTable.Id_title_back_img);
        mTitleTex = (Text) layout.findComponentById(ResourceTable.Id_title_text);
        mMenuImg = (Image) layout.findComponentById(ResourceTable.Id_title_menu_img);
        mMenuImg.setClickedListener(this);
        mBackImg.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                onBackClickListener.onBackClick();
            }
        });
        mLayout.setClickedListener(component -> {
            // 用于拦截底层的事件
        });
        mLayout.setLongClickedListener(new LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                // 用于拦截底层的事件
            }
        });
    }

    /**
     * 是否展示返回图标
     *
     * @param isShow 布尔值决定
     */
    public void setBackIconVisibility(boolean isShow) {
        if (isShow) {
            mBackImg.setVisibility(VISIBLE);
        } else {
            mBackImg.setVisibility(HIDE);
        }
    }

    /**
     * 设置title的名字
     *
     * @param title 名字
     */
    public void setTitle(String title) {
        mTitleTex.setText(title);
    }

    @Override
    public void onClick(Component component) {
        ListContainer menuListView = new ListContainer(mContext);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms_drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#DCDCDC")));
        menuListView.setBackground(background);
        TitleMenuProvider adapter = new TitleMenuProvider(mContext, getMenuData());
        menuListView.setItemProvider(adapter);
        menuListView.setItemClickedListener((parent, view, position, id) -> {
            if (mPopupWindow != null) {
                mPopupWindow.destroy();
                mPopupWindow = null;
            }
            mMenuItemClickedListener.onItemClicked(position);
        });
        mPopupWindow = new PopupDialog(getContext(), mMenuImg);
        mPopupWindow.setCustomComponent(menuListView);
        mPopupWindow.show();
        mPopupWindow.setDialogListener(() -> {
            if (mPopupWindow != null) {
                mPopupWindow.destroy();
                mPopupWindow = null;
            }
            return false;
        });
    }

    /**
     * 返回按钮接口
     *
     * @since 2021-03-27
     */
    public interface OnBackClickListener {
        /**
         * 回调返回按钮点击事件
         */
        void onBackClick();
    }

    /**
     * 设置返回点击事件回调监听
     *
     * @param onBackClickListener 回调接口
     */
    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
    }

    /**
     * 仿menu的item点击回调接口
     *
     * @since 2021-03-27
     */
    public interface MenuItemClickedListener {
        /**
         * 仿menu的item点击回调方法
         *
         * @param position item的position
         */
        void onItemClicked(int position);
    }

    /**
     * 设置menu item点击事件回调监听
     *
     * @param menuItemListener 回调接口
     */
    public void setMenuItemClickedListener(MenuItemClickedListener menuItemListener) {
        mMenuItemClickedListener = menuItemListener;
    }

    /**
     * 是否需要显示右上角Menu
     *
     * @param b true显示，flase不显示
     */
    public void isShowMenu(boolean b) {
        if (b) {
            mMenuImg.setVisibility(VISIBLE);
        } else {
            mMenuImg.setVisibility(HIDE);
        }
    }

    /**
     * 设置Menu列表的数据
     *
     * @return 数据列表
     */
    private List<String> getMenuData() {
        ArrayList<String> menuList = new ArrayList<>();
        menuList.add("Default Image");
        menuList.add("Failure Image");
        menuList.add("ViewPager");
        menuList.add("RecyclerView");
        menuList.add("Scale Step");
        menuList.add("Shared Element");
        return menuList;
    }
}
