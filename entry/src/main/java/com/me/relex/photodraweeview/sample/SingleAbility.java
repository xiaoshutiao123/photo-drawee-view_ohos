package com.me.relex.photodraweeview.sample;

import com.me.relex.photodraweeview.sample.slice.SingleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * SingleAbility
 *
 * @since 2021-03-27
 */
public class SingleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SingleAbilitySlice.class.getName());
    }
}
