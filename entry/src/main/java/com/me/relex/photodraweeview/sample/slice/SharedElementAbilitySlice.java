/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.slice;

import com.me.relex.photodraweeview.sample.CustomTitle;
import com.me.relex.photodraweeview.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

/**
 * SharedElement slice
 *
 * @since 2021-03-27
 */
public class SharedElementAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_shared_element);

        CustomTitle customTitle = (CustomTitle) findComponentById(ResourceTable.Id_custom_title);
        customTitle.setBackIconVisibility(true);
        customTitle.setTitle("Share Element");
        customTitle.setOnBackClickListener(() -> terminate());

        Image shareImg = (Image) findComponentById(ResourceTable.Id_share_img);
        shareImg.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        present(new SharedElementLaunchedAbilitySlice(), new Intent());
    }
}
