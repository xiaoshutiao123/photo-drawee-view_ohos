/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.slice;

import com.me.relex.photodraweeview.sample.CustomTitle;
import com.me.relex.photodraweeview.sample.ResourceTable;
import com.me.relex.photodraweeview.sample.provider.ImageProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * RecyclerView slice
 *
 * @since 2021-03-27
 */
public class RecyclerViewAbilitySlice extends AbilitySlice implements ImageProvider.InterceptionListener {
    private final List<Integer> imgs = new ArrayList<>();
    private ListContainer mRecyclerView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_transition);

        CustomTitle customTitle = (CustomTitle) findComponentById(ResourceTable.Id_custom_title);
        customTitle.setBackIconVisibility(true);
        customTitle.setTitle("RecyclerView");
        customTitle.setOnBackClickListener(() -> terminate());

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_list);
        imgs.add(ResourceTable.Media_viewpager_1);
        imgs.add(ResourceTable.Media_viewpager_2);
        imgs.add(ResourceTable.Media_viewpager_3);
        imgs.add(ResourceTable.Media_wallpaper);
        imgs.add(ResourceTable.Media_panda);
        ImageProvider imageProvider = new ImageProvider(this, imgs);
        imageProvider.setInterceptionListener(this::interceptPageSlider);
        mRecyclerView.setItemProvider(imageProvider);
        mRecyclerView.setItemClickedListener((listContainer, component, i, l) -> transition());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void transition() {
        Intent intent = new Intent();
        Operation option = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.github.chrisbanes.photoview.sample")
                .withAbilityName("com.github.chrisbanes.photoview.sample.SimpleSampleAbility")
                .build();
        intent.setOperation(option);
        startAbility(intent);
    }

    @Override
    public void interceptPageSlider(boolean isSlide) {
        if (isSlide == mRecyclerView.isEnabled()) {
            return;
        }
        mRecyclerView.setEnabled(isSlide);
    }
}
