package com.me.relex.photodraweeview.sample;

import com.me.relex.photodraweeview.sample.slice.ViewPagerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * ViewPagerAbility
 *
 * @since 2021-03-27
 */
public class ViewPagerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ViewPagerAbilitySlice.class.getName());
    }
}
