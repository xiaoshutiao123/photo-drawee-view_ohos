/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.provider;

import com.me.relex.photodraweeview.PhotoView;
import com.me.relex.photodraweeview.iml.OnEnableListener;
import com.me.relex.photodraweeview.sample.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.List;

/**
 * viewPager列表的provider
 *
 * @since 2021-03-27
 */
public class ViewPagerProvider extends PageSliderProvider implements OnEnableListener {
    private Context context;
    private List<Integer> imgs;
    private InterceptionListener mInterceptionListener;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param imgs    列表数据
     */
    public ViewPagerProvider(Context context, List<Integer> imgs) {
        this.context = context;
        this.imgs = imgs;
    }

    @Override
    public int getCount() {
        if (null == imgs || imgs.size() <= 0) {
            return 0;
        }
        return imgs.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_item_view_pager, null, false);
        PhotoView image = (PhotoView) componentContainer1.findComponentById(ResourceTable.Id_img);
        image.setPixelMap(imgs.get(i));

        // 检查componentContainer是否有显示并remove
        if (componentContainer.isComponentDisplayed()) {
            componentContainer.removeAllComponents();
        }
        image.isPageSlider(true);
        image.setOnEnableListener(this);
        componentContainer.addComponent(componentContainer1);
        return image;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }

    @Override
    public void onEnable(boolean isSlide) {
        mInterceptionListener.interceptPageSlider(isSlide);
    }

    /**
     * 判断是否PageSlider可以滑动的接口
     *
     * @since 2021-03-27
     */
    public interface InterceptionListener {
        /**
         * 判断是否PageSlider可以滑动的方法
         *
         * @param isSlide 是否参数
         */
        void interceptPageSlider(boolean isSlide);
    }

    /**
     * 设置是否PageSlider可以滑动
     *
     * @param interception 回调接口
     */
    public void setInterceptionListener(InterceptionListener interception) {
        mInterceptionListener = interception;
    }
}
