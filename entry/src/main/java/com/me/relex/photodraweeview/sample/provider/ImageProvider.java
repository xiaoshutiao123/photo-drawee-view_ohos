/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.provider;

import com.me.relex.photodraweeview.PhotoView;
import com.me.relex.photodraweeview.iml.OnEnableListener;
import com.me.relex.photodraweeview.sample.ResourceTable;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.List;

/**
 * recyclerView列表的provider
 *
 * @since 2021-03-27
 */
public class ImageProvider extends BaseItemProvider implements OnEnableListener {
    private final Context context;
    private final List<Integer> imgs;
    private ViewPagerProvider.InterceptionListener mInterceptionListener;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param imgs    列表数据
     */
    public ImageProvider(Context context, List<Integer> imgs) {
        this.context = context;
        this.imgs = imgs;
    }

    @Override
    public int getCount() {
        return imgs.size();
    }

    @Override
    public Object getItem(int i) {
        return imgs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHodler viewHolder;

        // component相当于原库中的view，其他的和原库中ListView的适配器adapter差不多。
        // 名字区别也不大，不过原库中ListView基本被淘汰了。
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_img, null, false);
            viewHolder = new ViewHodler();
            viewHolder.image = (PhotoView) component.findComponentById(ResourceTable.Id_img);
            viewHolder.image.isRecyclerView(true);
            viewHolder.image.setOnEnableListener(this);
            viewHolder.image.setImageAndDecodeBounds(imgs.get(i));
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHodler) component.getTag();
            viewHolder.image.setBindStateChangedListener(new Component.BindStateChangedListener() {
                @Override
                public void onComponentBoundToWindow(Component component) {
                    viewHolder.image.initPhotoView(component);
                }

                @Override
                public void onComponentUnboundFromWindow(Component component) {
                }
            });
        }
        return component;
    }

    @Override
    public void onEnable(boolean isSlide) {
        mInterceptionListener.interceptPageSlider(isSlide);
    }

    /**
     * viewHolder
     *
     * @since 2021-06-07
     */
    private static class ViewHodler {
        private PhotoView image;
    }

    /**
     * 判断是否PageSlider可以滑动的接口
     *
     * @since 2021-03-27
     */
    public interface InterceptionListener {
        /**
         * 判断是否PageSlider可以滑动的方法
         *
         * @param isSlide 布尔值
         */
        void interceptPageSlider(boolean isSlide);
    }

    /**
     * 设置是否PageSlider可以滑动
     *
     * @param interception 回调接口
     */
    public void setInterceptionListener(ViewPagerProvider.InterceptionListener interception) {
        mInterceptionListener = interception;
    }
}
