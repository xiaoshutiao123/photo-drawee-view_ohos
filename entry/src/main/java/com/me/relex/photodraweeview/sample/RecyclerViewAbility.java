package com.me.relex.photodraweeview.sample;

import com.me.relex.photodraweeview.sample.slice.RecyclerViewAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * RecyclerViewAbility
 *
 * @since 2021-03-27
 */
public class RecyclerViewAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RecyclerViewAbilitySlice.class.getName());
    }
}
