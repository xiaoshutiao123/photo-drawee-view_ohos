/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.sample.slice;

import com.me.relex.photodraweeview.sample.CustomTitle;
import com.me.relex.photodraweeview.sample.ResourceTable;
import com.me.relex.photodraweeview.sample.provider.ViewPagerProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;

import java.util.ArrayList;
import java.util.List;

/**
 * viewpager slice
 *
 * @since 2021-03-27
 */
public class ViewPagerAbilitySlice extends AbilitySlice implements ViewPagerProvider.InterceptionListener {
    private static final int INDICATOR_HEIGHT_WIDTH = 20;
    private static final int INDICATOR_RADIUS = 10;
    private static final int INDICATOR_RGB_COLOR = 190;

    private List<Integer> imgs = new ArrayList<>();

    private PageSlider mPageSlider;
    private DirectionalLayout mLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_viewpager);

        CustomTitle customTitle = (CustomTitle) findComponentById(ResourceTable.Id_custom_title);
        customTitle.setBackIconVisibility(true);
        customTitle.setTitle("ViewPager");
        customTitle.setOnBackClickListener(() -> terminate());

        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_id_PageSlider);
        mLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_layout_directional);

        imgs.add(ResourceTable.Media_panda);
        imgs.add(ResourceTable.Media_wallpaper);
        imgs.add(ResourceTable.Media_viewpager_1);
        imgs.add(ResourceTable.Media_viewpager_2);
        imgs.add(ResourceTable.Media_viewpager_3);
        ViewPagerProvider imageProvider = new ViewPagerProvider(this, imgs);
        imageProvider.setInterceptionListener(this);
        mPageSlider.setProvider(imageProvider);

        getIndicator(mPageSlider.getCurrentPage());
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void interceptPageSlider(boolean isSlide) {
        if (isSlide != mPageSlider.isEnabled()) {
            mPageSlider.setEnabled(isSlide);
        }
        getIndicator(mPageSlider.getCurrentPage());
    }

    /**
     * 添加viewpager的切换监听并设置底部提示
     *
     * @param position 当前切换到viewpager的position
     */
    private void getIndicator(int position) {
        mLayout.removeAllComponents();
        for (int i = 0; i < imgs.size(); i++) {
            // 为组件添加对应布局的布局属性
            DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                    ComponentContainer.LayoutConfig.MATCH_CONTENT,
                    ComponentContainer.LayoutConfig.MATCH_CONTENT);
            layoutConfig.alignment = LayoutAlignment.CENTER;

            Image image = new Image(getContext());
            image.setLayoutConfig(layoutConfig);
            ShapeElement background = new ShapeElement();
            if (position == i) {
                image.setWidth(INDICATOR_HEIGHT_WIDTH);
                image.setHeight(INDICATOR_HEIGHT_WIDTH);
                background.setRgbColor(new RgbColor(0, 0, 0));
                background.setCornerRadius(INDICATOR_RADIUS);
            } else {
                image.setWidth(INDICATOR_HEIGHT_WIDTH);
                image.setHeight(INDICATOR_HEIGHT_WIDTH);
                background.setRgbColor(new RgbColor(INDICATOR_RGB_COLOR, INDICATOR_RGB_COLOR, INDICATOR_RGB_COLOR));
                background.setCornerRadius(INDICATOR_RADIUS);
            }
            image.setBackground(background);
            image.setPadding(INDICATOR_RADIUS, INDICATOR_RADIUS, INDICATOR_RADIUS, INDICATOR_RADIUS);
            mLayout.addComponent(image);
            mLayout.setAlignment(LayoutAlignment.CENTER);
        }
    }
}
