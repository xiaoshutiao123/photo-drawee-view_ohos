/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview.utils;

import java.text.DecimalFormat;

/**
 * 工具类
 *
 * @since 2021-03-27
 */
public class Util {
    /**
     * 转换float数据为保留固定小数位的String字符
     *
     * @param value   要转换数值
     * @param pattern 格式
     * @return 转换的字符串
     */
    public static String formatFloatToStr(float value, String pattern) {
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(value);
    }
}
