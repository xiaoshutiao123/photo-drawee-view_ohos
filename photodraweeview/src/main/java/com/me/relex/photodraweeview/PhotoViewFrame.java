/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.me.relex.photodraweeview;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * 自定义StackLayout子控件包括PhotoView
 *
 * @since 2021-03-27
 */
public class PhotoViewFrame extends DirectionalLayout implements Component.LongClickedListener,
        Component.ClickedListener {
    private static final int TOAST_SHOW_DURATION = 2 * 1000;
    private static final int TOAST_SHOW_Y = 200;

    private PhotoView photoView;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public PhotoViewFrame(Context context) {
        super(context);
        mContext = context;
        initView(context);
        setAlignment(LayoutAlignment.CENTER);
    }

    private void initView(Context context) {
        photoView = (PhotoView) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_photo_frame,
                null, false);
        photoView.setLongClickedListener(this);
        photoView.setClickedListener(this);
        addComponent(photoView);
    }

    /**
     * 设置图片资源文件
     *
     * @param resId 资源文件
     */
    public void setImageAndDecodeBounds(int resId) {
        photoView.setImageAndDecodeBounds(resId);
    }

    /**
     * 设置图片scale
     *
     * @param scaleStep scale倍数
     */
    public void setScaleStep(float scaleStep) {
        photoView.setScaleStep(scaleStep);
    }

    /**
     * 重置图片
     */
    public void initPhotoView() {
        photoView.initPhotoView(photoView);
    }

    /**
     * 上层传递的双击事件
     */
    public void doubleClick() {
        photoView.doubleClick(photoView);
    }

    @Override
    public void onLongClicked(Component component) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText("onLongClicked");
        new ToastDialog(getContext())
                .setComponent(toastLayout)
                .setSize(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.BOTTOM)
                .setDuration(TOAST_SHOW_DURATION)
                .setOffset(0, TOAST_SHOW_Y)
                .show();
    }

    @Override
    public void onClick(Component component) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(mContext)
                .parse(ResourceTable.Layout_layout_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText("onPhotoTap:x=" + photoView.getTouchUpX() + ";y=" + photoView.getTouchUpY(getTop()));
        new ToastDialog(getContext())
                .setComponent(toastLayout)
                .setSize(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.BOTTOM)
                .setDuration(TOAST_SHOW_DURATION)
                .setOffset(0, TOAST_SHOW_Y)
                .show();
    }
}
